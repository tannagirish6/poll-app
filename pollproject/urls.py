from django.contrib import admin
from django.urls import path
from pollapp.views import uhome,usignup,uwelcome,ulogout,ucreate,uvote,viewresults
urlpatterns = [
    path('admin/', admin.site.urls),
    path("",uhome,name="uhome"),
    path("usignup",usignup,name="usignup"),
    path("uwelcome",uwelcome,name="uwelcome"),
    path("ucreate",ucreate,name="ucreate"),
    path("ulogout",ulogout,name="ulogout"),
    path("uvote/<int:i>",uvote,name="uvote"),
    path("viewresults/<int:i>",viewresults,name="viewresults")
]
