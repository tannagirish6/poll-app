from django.db import models

class QuestionModel(models.Model):
    id = models.AutoField(primary_key=True)
    pq = models.CharField(max_length=100)
    o1 = models.CharField(max_length=100)
    o2 = models.CharField(max_length=100)
    o3 = models.CharField(max_length=100)
    o1_cnt = models.IntegerField()
    o2_cnt = models.IntegerField()
    o3_cnt = models.IntegerField()
    def __str__(self):
        return str(self.pq)+" "+str(self.id)

