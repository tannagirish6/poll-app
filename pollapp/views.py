from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from .models import QuestionModel
def uhome(request):
    if request.user.is_authenticated:
        return redirect("uwelcome")
    else:
        if request.method == "POST":
            un = request.POST.get("un")
            pw = request.POST.get("pw")
            usr = authenticate(username=un,password=pw)
            print("hwllo")
            if usr is None:
                return render(request,"uhome.html",{"msg":"invalid username / password"})
            else:
                login(request,usr)
                return redirect("uwelcome")
        else:
            return render(request,"uhome.html")
def usignup(request):
    if request.user.is_authenticated:
        return redirect("uwelcome")
    else:
        if request.method == "POST":
            un = request.POST.get("un")
            pw1 = request.POST.get("pw1")
            pw2 = request.POST.get("pw2")
            if pw1 == pw2:
                try:
                    usr = User.objects.get(username=un)
                    return render(request,"usignup.html",{"msg":"user already registered"})
                except:
                    usr = User.objects.create_user(username=un,password=pw1)
                    usr.save() 
                    return redirect("uhome") 
            else:
                return render(request,"usignup.html",{"msg":"passwords did not match"})
        else:
            return render(request,"usignup.html")
def uwelcome(request):
    if request.user.is_authenticated:
        data = QuestionModel.objects.all()
        return render(request,"uwelcome.html",{"data":data})
    else:
        return redirect("uhome")

def ulogout(request):
    logout(request)
    return redirect("uhome")
def ucreate(request):
    if request.method == "POST":
        poleq = request.POST.get("pq")
        opt1 =  request.POST.get("o1")
        opt2 =  request.POST.get("o2")
        opt3 =  request.POST.get("o3")
        data = QuestionModel(pq = poleq,o1 = opt1,o2=opt2,o3=opt3,o1_cnt=0,o2_cnt=0,o3_cnt=0)
        data.save()
        msg = "Question Saved Thankyou!"
        return render(request,"ucreate.html",{"msg":msg})
    else:
        
        return render(request,"ucreate.html")
def uvote(request,i):
    if request.method == "GET":
        try:
            op = int(request.GET.get("s"))
            up = QuestionModel.objects.get(id=i)
            if op == 1:
                up.o1_cnt += 1
            if op == 2:
                up.o2_cnt += 1
            if op == 3:
                up.o3_cnt += 1
            up.save()
            return redirect("uhome")
        except:
            st = QuestionModel.objects.get(id=i)
            return render(request,"uvote.html",{"data":st})
def viewresults(request,i):
    data = QuestionModel.objects.get(id=i)
    return render(request,"viewresults.html",{"data":data})