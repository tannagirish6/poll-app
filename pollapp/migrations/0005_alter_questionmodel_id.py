# Generated by Django 4.1.7 on 2023-03-22 14:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pollapp', '0004_remove_questionmodel_pq_id_questionmodel_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questionmodel',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
